﻿using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private Vector3 velocity;
    [SerializeField] private Vector3 angularVelocity;

    [SerializeField] private Vector3 torque;

    private void Update()
    {
        var rigid = GetComponent<Rigidbody>();
        velocity = rigid.velocity;
        angularVelocity = rigid.angularVelocity;
    }
}
