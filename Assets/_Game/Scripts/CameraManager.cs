﻿using Assets.UnityCommonUtilities;
using UnityEngine;

public class CameraManager : UnitySingleton<CameraManager>
{
    [SerializeField] private GameObject followCameraRig;
    [SerializeField] private GameObject rotationCameraRig;
    [SerializeField] private ObjectShooter objectShooter;

    [SerializeField] private bool followFiredObject;

    public GameObject CameraFollow => followCameraRig;
    public GameObject CameraRotation => rotationCameraRig;

    private void Start()
    {
        rotationCameraRig.SetActive(true);
        followCameraRig.SetActive(false);

        objectShooter.OnObjectFired += OnObjectFired;
    }

    private void OnObjectFired(GameObject obj)
    {
        if (followFiredObject)
        {
            rotationCameraRig.gameObject.SetActive(false);

            followCameraRig.transform.position = obj.transform.position;
            followCameraRig.transform.rotation = obj.transform.rotation;
            followCameraRig.gameObject.SetActive(true);
            followCameraRig.GetComponent<PositionFollow>().Target = obj.transform;
            obj.GetComponent<CollisionListener>().CollisionEnter += OnObjectCollisionEnter;
            obj.GetComponent<FiredObject>().OnObjectDestroy += OnObjectDestroy;
        }
    }

    private void OnObjectDestroy()
    {
        rotationCameraRig.SetActive(true);
        followCameraRig.SetActive(false);
    }

    private void OnObjectCollisionEnter(Collision collision)
    {
        //Stop following object?
    }
}
