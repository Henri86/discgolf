﻿using System;
using UnityEngine;

public class FiredObject : MonoBehaviour
{
    public Action OnObjectDestroy;

    private void Start()
    {
        
    }

    private void OnDestroy()
    {
        OnObjectDestroy?.Invoke();
    }
}
