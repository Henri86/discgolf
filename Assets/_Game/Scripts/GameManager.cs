﻿using UnityEngine;
using UnityTemplateProjects;

public class GameManager : MonoBehaviour
{
    [SerializeField] private ObjectShooter shooter;
    [SerializeField] private SimpleCameraController cameraController;

    private void Start()
    {
        
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            shooter.RotationEnabled = !shooter.RotationEnabled;
            cameraController.enabled = !shooter.RotationEnabled;
        }
    }
}
