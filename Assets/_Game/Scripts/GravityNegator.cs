﻿using UnityEngine;

public class GravityNegator : MonoBehaviour
{
    [SerializeField] [Range(0, 1)] public float amount = 0;

    private Rigidbody Rigidbody => GetComponent<Rigidbody>();

    private void Start()
    {
        
    }

    private void FixedUpdate()
    {
        var t = Physics.gravity * Rigidbody.mass;
        t *= amount;
        Rigidbody.AddForce(-t);
    }
}
