﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectShooter : MonoBehaviour
{
    [SerializeField] private GameObject objectToShoot;

    [SerializeField] private float force = 1;
    [SerializeField] private Vector3 initialTorque;

    [SerializeField] private float verticalRotationSpeed = 1;
    [SerializeField] private float horizontalRotationSpeed = 1;
    [SerializeField] private float forceChangeSpeed = 1;
    [SerializeField] private float destroyObjectAfterCollisionInTime = 1f;
    [SerializeField] private float destroyObjectInTime = 6f;

    [SerializeField] private Transform pipeRotationCenter;
    [SerializeField] private Transform pipeHolder;
    [SerializeField] private Transform objectSpawnPosition;
    
    [SerializeField] private float desiredHorizontalRotation;
    [SerializeField] private float currentHorizontalRotation;
    [SerializeField] private float desiredVerticalRotation;
    [SerializeField] private float currentVerticalRotation;

    public bool RotationEnabled { get; set; } = true;

    public Action<GameObject> OnObjectFired;

    private void Start()
    {

    }

    private void Update()
    {
        HandleInput();
        HandleRotation();
    }

    private void HandleRotation()
    {
        pipeRotationCenter.localEulerAngles = new Vector3(desiredVerticalRotation, 0, 0);
        pipeHolder.localEulerAngles = new Vector3(0, desiredHorizontalRotation, 0);
    }

    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            Shoot();

        if (RotationEnabled)
        {
            if (Input.GetKey(KeyCode.S))
                desiredVerticalRotation += verticalRotationSpeed * Time.deltaTime;
            if (Input.GetKey(KeyCode.W))
                desiredVerticalRotation -= verticalRotationSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.A))
                desiredHorizontalRotation -= horizontalRotationSpeed * Time.deltaTime;
            if (Input.GetKey(KeyCode.D))
                desiredHorizontalRotation += horizontalRotationSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.Q))
            force -= forceChangeSpeed * Time.deltaTime;
        if (Input.GetKey(KeyCode.E))
            force += forceChangeSpeed * Time.deltaTime;
    }

    private void Shoot()
    {
        if (objectToShoot == null)
        {
            Debug.LogWarning("Object to shoot not specified.");
            return;
        }

        var createdObject = Instantiate(objectToShoot, objectSpawnPosition.position, objectSpawnPosition.rotation);
        createdObject.AddComponent<FiredObject>();
        var rigid = createdObject.GetComponent<Rigidbody>() ?? createdObject.AddComponent<Rigidbody>();

        rigid.AddForce(objectSpawnPosition.forward.normalized * force, ForceMode.Impulse);

        rigid.AddTorque(initialTorque, ForceMode.Impulse);

        var collisionListener = createdObject.AddComponent<CollisionListener>();
        collisionListener.CollisionEnter += (collision) => StartCoroutine(WaitAndDestroy(createdObject, destroyObjectAfterCollisionInTime));
        StartCoroutine(WaitAndDestroy(createdObject, destroyObjectInTime));

        var audioSource = GetComponent<AudioSource>();
        audioSource.pitch = UnityEngine.Random.Range(0.8f, 1.3f);
        audioSource.Play();

        OnObjectFired?.Invoke(createdObject);
    }

    private IEnumerator WaitAndDestroy(GameObject createdObject, float seconds, Action onDestroy = null)
    {
        while (true)
        {
            yield return new WaitForSeconds(seconds);
            Destroy(createdObject);
            onDestroy?.Invoke();
        }
    }
}
