﻿using UnityEngine;

public class PositionFollow : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private Vector3 offset;
    [SerializeField] private float smoothTime = 0.3F;

    private Vector3 velocity = Vector3.zero;

    public Transform Target
    {
        get => target;
        set => target = value;
    }

    private void Update()
    {
        if (target != null)
        {
            transform.position = Vector3.SmoothDamp(transform.position, target.position + offset, ref velocity, smoothTime);
        }
    }
}
