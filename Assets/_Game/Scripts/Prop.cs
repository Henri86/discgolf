﻿using Assets.UnityCommonUtilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Prop : MonoBehaviour
{
    [SerializeField] private AudioClip[] hitSounds;

    [SerializeField]
    [Range(0.0f, 2.0f)]
    public float minPitch = 0.8f;
    [SerializeField]
    [Range(0.0f, 2.0f)]
    public float maxPitch = 1.3f;

    [SerializeField] public float soundPlayCooldown = 0.2f;
    [SerializeField] public float minImpactForSound = 20;
    [SerializeField] public float maxImpactForSound = 150;

    public float recentImpact;

    private bool audioOnCooldown;

    private void OnCollisionEnter(Collision collision)
    {
        float collisionForce = collision.impulse.magnitude / Time.fixedDeltaTime;
        recentImpact = collisionForce;

        if (hitSounds.Length > 0 && !audioOnCooldown)
        {
            var source = GetComponent<AudioSource>();
            var volume = MathUtils.MapValue(minImpactForSound, maxImpactForSound, 0, 1, collisionForce);
            if (volume >= 0.1)
            {
                source.clip = hitSounds[Random.Range(0, hitSounds.Length - 1)];
                source.pitch = Random.Range(minPitch, maxPitch);
                source.volume = volume;
                source.Play();
                audioOnCooldown = true;
            }
        }        

        StartCoroutine(WaitAndDisableCooldown(soundPlayCooldown));
    }

    private IEnumerator WaitAndDisableCooldown(float seconds)
    {
        while (true)
        {
            yield return new WaitForSeconds(seconds);
            audioOnCooldown = false;
        }
    }
}
