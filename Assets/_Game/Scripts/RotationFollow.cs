﻿using UnityEngine;

public class RotationFollow : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float smoothTime = 0.3F;

    private Quaternion rotation = Quaternion.identity;

    private void Update()
    {
        transform.rotation = QuaternionUtil.SmoothDamp(transform.rotation, target.rotation, ref rotation, smoothTime);
    }
}
